import java.util.Scanner;

public class activity {
    public static void main(String[] args) {

        String firstName;
        String lastName;
        double firstSubject ;
        double secondSubject  ;
        double thirdSubject  ;

        Scanner input = new Scanner(System.in);


        System.out.println("First Name: ");
        firstName = input.nextLine();

        System.out.println("Last Name: ");
        lastName = input.nextLine();

        System.out.println("First Subject Grade: ");
        firstSubject = new Double(input.nextLine());

        System.out.println("Second Subject Grade: ");
        secondSubject = new Double(input.nextLine());

        System.out.println("Third Subject Grade: ");
        thirdSubject = new Double(input.nextLine());

        double avg = (firstSubject + secondSubject + thirdSubject)/3;

        String output = String.format("Good day, %s %s. Your grade average is: %.2f", firstName, lastName, avg);

        System.out.println(output);
    }
}
